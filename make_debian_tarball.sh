#!/bin/bash

mkdir builds
cd builds

COMPONENT=xdais
VERSION=7_24_00_04

RELEASE=${COMPONENT}_${VERSION}
COMPONENT_PPA=${COMPONENT}ppa

wget --no-proxy http://downloads.ti.com/dsps/dsps_public_sw/sdo_sb/targetcontent/$COMPONENT/$VERSION/exports/${RELEASE}.tar.gz
mv $RELEASE.tar.gz* $RELEASE.tar.gz
tar -xzvf $RELEASE.tar.gz

git clone git://git.ti.com/mfp/${COMPONENT_PPA}.git

cd $RELEASE

cp -r ../$COMPONENT_PPA/debian .
#cp ../$COMPONENT_PPA/Makefile .

cd ..

tar -czvf ${RELEASE}_ppa.tar.gz $RELEASE

